import uuid

import requests
from faker import Faker
import random

fake = Faker('pl_PL')


def generate_random_gender():
    gender = random.choice(['male', 'female'])
    return gender


def generate_random_male_or_female_firstname(gender):
    if gender == ['male']:
        return fake.first_name_male()
    else:
        return fake.first_name_female()


def generate_random_user_data():
    gender = generate_random_gender()
    first_name = generate_random_male_or_female_firstname(gender)
    last_name = fake.last_name()
    user_data = {
        "name": f"{first_name} {last_name}",
        "email": f"{first_name}.{last_name}{uuid.uuid4()}@example.com",
        "gender": gender,
        "status": random.choice(['active', 'inactive'])
    }
    return user_data


class GoRestHandler:
    base_url = "https://gorest.co.in/public/v2"
    user_endpoint = "/users"
    headers = {
        "Authorization": "Bearer f6db2c588d2c4a259f528d71f43094381bc2f860ffe59e34880ee17a8c1883b6"
    }

    def create_user(self, user_data):
        response = requests.post(self.base_url + self.user_endpoint, json=user_data, headers=self.headers)
        assert response.status_code == 201
        return response

    def get_user_by_id(self, user_id):
        response = requests.get(f"{self.base_url}{self.user_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 200
        return response

    def update_user(self, user_id, user_data):
        response = requests.patch(f"{self.base_url}{self.user_endpoint}/{user_id}", json=user_data, headers=self.headers)
        assert response.status_code == 200
        return response

    def delete_user(self, user_id):
        response = requests.delete(f"{self.base_url}{self.user_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 204
        return response

    def get_deleted_user(self, user_id):
        response = requests.get(f"{self.base_url}{self.user_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 404
        return response
