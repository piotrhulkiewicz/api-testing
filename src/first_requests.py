import pytest
import requests


def get_my_ip():
    res = requests.get("https://api.ipify.org?format=json")
    status_code = res.status_code
    body = res.json()
    print(status_code)
    print(body)
    return res
