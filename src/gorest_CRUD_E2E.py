import random
import requests
import sys
from faker import Faker

fake = Faker(['pl-PL'])


def create_random_user_data():
    gender = random.choice(['Male', 'Female'])
    name = [fake.first_name_male() if gender == 'Male' else fake.first_name_female(), fake.last_name()]
    user_data = {
        "name": f"{name[0]} {name[-1]}",
        "email": fake.email(),
        "gender": gender,
        "status": random.choice(['active', 'inactive'])
    }
    return user_data


def create_new_random_email():
    new_email_address = fake.email()
    new_email = {
        'email': new_email_address
    }
    return new_email


class GorestAPIHandler:
    base_url = 'https://gorest.co.in'
    user_endpoint = '/public/v2/users'
    modify_endpoint = '/public/v2/users/'

    def create_new_user(self, user_data):
        headers = {
            'Authorization': 'Bearer f6db2c588d2c4a259f528d71f43094381bc2f860ffe59e34880ee17a8c1883b6'
        }
        response = requests.post(self.base_url + self.user_endpoint, json=user_data, headers=headers)
        assert response.status_code == 201
        return response

    def read_user(self, id):
        headers = {
            'Authorization': 'Bearer f6db2c588d2c4a259f528d71f43094381bc2f860ffe59e34880ee17a8c1883b6'
        }
        response = requests.get(self.base_url + self.modify_endpoint + str(id), headers=headers)
        assert response.status_code == 200
        return response

    def get_deleted_user(self, id):
        headers = {
            'Authorization': 'Bearer f6db2c588d2c4a259f528d71f43094381bc2f860ffe59e34880ee17a8c1883b6'
        }
        response = requests.get(self.base_url + self.modify_endpoint + str(id), headers=headers)
        assert response.status_code == 404
        return response

    def update_user(self, id, new_email):
        headers = {
            'Authorization': 'Bearer f6db2c588d2c4a259f528d71f43094381bc2f860ffe59e34880ee17a8c1883b6'
        }
        response = requests.patch(self.base_url + self.modify_endpoint + str(id), json=new_email, headers=headers)
        assert response.status_code == 200
        return response

    def delete_user(self, id):
        headers = {
            'Authorization': 'Bearer f6db2c588d2c4a259f528d71f43094381bc2f860ffe59e34880ee17a8c1883b6'
        }
        response = requests.delete(self.base_url + self.modify_endpoint + str(id), headers=headers)
        assert response.status_code == 204
        return response


if __name__ == '__main__':
    user_choice = 0
    while user_choice not in range(1, 6):
        user_choice = int(input("Jaką czynność chcesz wykonać? Wybierz 1-5\n"
                                "1 - założenie nowego użytkownika w bazie\n"
                                "2 - wczytanie danych użytkowanika (podaj id)\n"
                                "3 - zmiana adresu email użytkownika (podaj id)\n"
                                "4 - usunięcie użytkownika (podaj id)\n"
                                "5 - wyjście z programu\n"))
    gorestAPI = GorestAPIHandler()
    if user_choice == 1:
        # CRUD - Tworzenie nowego użytkownika
        response = gorestAPI.create_new_user(create_random_user_data())
        body = response.json()
        print("nowy użytkownik został założony \n", body)

    elif user_choice == 2:
        # CRUD - Wyświetlanie danych użytkownika, wymaga podania id
        id = input("podaj id\n")
        response = gorestAPI.read_user(id)
        body = response.json()
        print(body)

    elif user_choice == 3:
        # CRUD - Aktualizacja adresu email użytkowanika, wymoga podania id
        id = input("podaj id\n")
        response = gorestAPI.update_user(id, create_new_random_email())
        body = response.json()
        print(body)

    elif user_choice == 4:
        # CRUD - Usunięcie użytkownika, wymaga podania id
        id = input("podaj id\n")
        response = gorestAPI.delete_user(id)
        print("usunięto użytkownika u numerze id:", id)
        print(response)
    else:
        sys.exit()
