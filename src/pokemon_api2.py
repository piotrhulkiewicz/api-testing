import requests


class PokemonApiHandler:
    base_url = "https://pokeapi.co/api/v2"
    pokemon_endpoint = "/pokemon"
    pokemon_shape_endpoint = "/pokemon-shape"

    def get_list_of_pokemons(self, params=None):
        response = requests.get(self.base_url + self.pokemon_endpoint, params=params)
        assert response.status_code == 200
        return response

    def get_list_of_pokemon_shapes(self):
        response = requests.get(self.base_url + self.pokemon_shape_endpoint)
        assert response.status_code == 200
        return response

    def get_any_name_of_pokemon_shapes(self, counter):
        response = requests.get(self.base_url + self.pokemon_shape_endpoint).json()
        name = response['results'][counter - 1]['name']
        return name

    def get_pokemon_info_filtered_by_name(self, name):
        return requests.get(f"{self.base_url + self.pokemon_shape_endpoint}/{name}")

