from src.gorest_api import GoRestHandler
from src.gorest_api import generate_random_user_data

gorest_handler = GoRestHandler()


def test_crud_gorest_api_users():
    # test create user
    user_data = generate_random_user_data()
    response = gorest_handler.create_user(user_data)
    body = response.json()
    assert "id" in body
    # test find user by id
    user_id = body['id']
    body = gorest_handler.get_user_by_id(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
    # test modify user data searched by id
    modified_user_data = generate_random_user_data()
    body = gorest_handler.update_user(user_id, modified_user_data).json()
    assert body["email"] == modified_user_data["email"]
    assert body["name"] == modified_user_data["name"]
    # test delete user
    gorest_handler.delete_user(user_id)
    response = gorest_handler.get_deleted_user(user_id)
    assert response.status_code == 404
    assert response.json()['message'] == "Resource not found"
