from src.pokemon_api2 import PokemonApiHandler

pokeapi_handler = PokemonApiHandler()
params = {
    "limit": 11,
    "offset": 20
}

response = pokeapi_handler.get_list_of_pokemons(params)


def test_response_is_not_empty():
    body = response.json()
    assert body  # sprawdza czy odpowiedz ma jakąś wartość


def test_status_code_is_ok():
    assert response.status_code == 200


def test_if_response_has_all_pokemons():
    params = {
        "limit": 3000,
        "offset": 0
    }
    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()
    assert body["count"] == len(body["results"])


def test_time_response_is_below_1_second():
    response_time_in_ms = response.elapsed.microseconds // 1000
    assert response_time_in_ms < 1000


def test_response_content_is_below_100_kilobytes():
    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100


def test_pagination_is_properly_implemented():
    counter = 0
    body = response.json()
    print(body["results"][0]["url"])
    assert body["results"][0][
               "url"] == f"{pokeapi_handler.base_url}{pokeapi_handler.pokemon_endpoint}/{params['offset'] + 1}/"
    assert body["results"][-1][
               "url"] == f"{pokeapi_handler.base_url}{pokeapi_handler.pokemon_endpoint}/{params['offset'] + params['limit']}/"
    assert len(body["results"]) == params["limit"]
    for result in body["results"]:
        assert result[
                   "url"] == f"{pokeapi_handler.base_url}{pokeapi_handler.pokemon_endpoint}/{params['offset'] + 1 + counter}/"
        counter += 1


def test_list_of_pokemon_shapes_returns_properly():
    response = pokeapi_handler.get_list_of_pokemon_shapes().json()
    assert response["count"] == len(response["results"])


def test_if_pokemon_shape_name_returns_correctly():
    counter = 3
    response = pokeapi_handler.get_list_of_pokemon_shapes().json()
    name = pokeapi_handler.get_any_name_of_pokemon_shapes(counter)
    assert name == response["results"][counter - 1]["name"]
    response = pokeapi_handler.get_pokemon_info_filtered_by_name(name).json()
    assert response['id'] == counter
