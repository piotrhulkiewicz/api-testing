from src.first_requests import get_my_ip


def test_status_code():
    res = get_my_ip()
    status_code = res.status_code
    body = res.json()
    print(status_code)
    print(body)
    assert status_code == 200
