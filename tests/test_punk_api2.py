import requests


def test_default_beers():
    res = requests.get("https://api.punkapi.com/v2/beers")
    status_code = res.status_code
    body = res.json()
    assert status_code == 200
    assert body[0]['id'] == 1
    assert body[-1]['id'] == 25
    assert len(body) == 25


def test_get_beer_with_id_123():
    params = {
        "ids": 123
    }
    res = requests.get("https://api.punkapi.com/v2/beers", params=params)
    status_code = res.status_code
    body = res.json()
    assert status_code == 200
    assert body[0]['id'] == 123


def test_get_beers_with_id_from_11_to_20():
    params = {
        "ids": "11|12|13|14|15|16|17|18|19|20"
    }
    res = requests.get("https://api.punkapi.com/v2/beers", params=params)
    status_code = res.status_code
    body = res.json()
    beer_id = 11
    for beer in body:
        assert beer['id'] == beer_id
        beer_id += 1
    assert len(body) == 10
    assert status_code == 200


def test_get_20_beers_from_page_5():
    params = {
        "page": 5,
        "per_page": 20
    }
    res = requests.get("https://api.punkapi.com/v2/beers", params=params)
    status_code = res.status_code
    body = res.json()
    assert body[0]["id"] == 81
    assert body[-1]["id"] == 100
    assert len(body) == 20
    assert status_code == 200


def test_get_all_beers_with_abv_in_range_5_to_7():
    params = {
        "abv_gt": 4.9,
        "abv_lt": 7.1
    }
    res = requests.get("https://api.punkapi.com/v2/beers", params=params)
    status_code = res.status_code
    body = res.json()
    abv_list = []
    for beer in body:
        abv_list.append(beer["abv"])
    assert status_code == 200
    for abv in abv_list:
        assert 5 <= abv <= 7


def test_get_beers_brewed_in_2010():
    params = {
        "brewed_after": "12-2009",
        "brewed_before": "01-2011"
    }
    res = requests.get("https://api.punkapi.com/v2/beers", params=params)
    status_code = res.status_code
    body = res.json()
    assert status_code == 200
    for beer in body:
        assert "2010" in beer["first_brewed"]
